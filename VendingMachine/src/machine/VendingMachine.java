package machine;

/**
 * Encapsulates the state of a vending machine and the operations that can be
 * performed on it
 */
public class VendingMachine {

	private boolean isMachineOn;

	private int moneyInserted;

	private Change change;

	public VendingMachine() {
		super();
		// Initialise the money inserted to 0.
		moneyInserted = 0;
		change = Change.getInstance();
	}

	public boolean isOn() {
		return isMachineOn;
	}

	public void setOn() {
		isMachineOn = true;
	}

	public void setOff() {
		isMachineOn = false;
	}

	public int insertMoney(MoneyEnum money) {

		if (isMachineOn) {
			switch (money) {
			case TENPENCE:
				moneyInserted += money.getNumVal();
				change.incrementTenPence();
				break;
			case TWENTYPENCE:
				moneyInserted += money.getNumVal();
				change.incrementTwentyPence();
				break;
			case FIFTYPENCE:
				moneyInserted += money.getNumVal();
				change.incrementFiftyPence();
				break;
			case ONEPOUND:
				moneyInserted += money.getNumVal();
				change.incrementOnePound();
				break;
			default:
				System.out.println("Invalid money amount entered. Try TENPENCE, TWENTYPENCE, FIFTYPENCE, or ONEPOUND.");
				break;
			}
		}
		return moneyInserted;
	}

	public int returnMoney() {
		int returnedMoney = 0;
		if (isMachineOn) {
			returnedMoney = moneyInserted;
			while (moneyInserted > 0) {
				if (change.getOnePound() > 0 && ((moneyInserted - MoneyEnum.ONEPOUND.getNumVal()) >= 0)) {
					moneyInserted -= MoneyEnum.ONEPOUND.getNumVal();
					change.decrementOnePound();
				} else if (change.getFiftyPence() > 0 && ((moneyInserted - MoneyEnum.FIFTYPENCE.getNumVal()) >= 0)) {
					moneyInserted -= MoneyEnum.FIFTYPENCE.getNumVal();
					change.decrementFiftyPence();
				} else if (change.getTwentyPence() > 0 && ((moneyInserted - MoneyEnum.TWENTYPENCE.getNumVal()) >= 0)) {
					moneyInserted -= MoneyEnum.TWENTYPENCE.getNumVal();
					change.decrementTwentyPence();
				} else if (change.getTenPence() > 0 && ((moneyInserted - MoneyEnum.TENPENCE.getNumVal()) >= 0)) {
					moneyInserted -= MoneyEnum.TENPENCE.getNumVal();
					change.decrementTwentyPence();
				}
			}
		}
		return returnedMoney;
	}

	public int buyItem(ItemsEnum item) {
		if (isMachineOn) {
			switch (item) {
			case ITEM_A:
			case ITEM_B:
			case ITEM_C:
				if (item.getItemCost() > moneyInserted) {
					System.out.print("You have not inserted enough money for this item.");
				} else {
					moneyInserted -= item.getItemCost();
				}
				break;
			default:
				System.out.print("You have not chosen a valid item.");
				break;
			}
		}
		return returnMoney();
	}

}