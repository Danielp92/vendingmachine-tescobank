package machine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {

	@Test
	public void defaultStateIsOff() {
		VendingMachine machine = new VendingMachine();
		assertFalse(machine.isOn());
	}

	@Test
	public void turnsOn() {
		VendingMachine machine = createVendingMachineOn();
		assertTrue(machine.isOn());
	}

	@Test
	public void turnsOff() {
		VendingMachine machine = createVendingMachineOn();
		machine.setOff();
		assertFalse(machine.isOn());
	}

	@Test
	public void insertMoney() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 0;
		moneyInserted += MoneyEnum.TENPENCE.getNumVal();
		assertTrue(machine.insertMoney(MoneyEnum.TENPENCE) == moneyInserted);
		moneyInserted += MoneyEnum.TWENTYPENCE.getNumVal();
		assertTrue(machine.insertMoney(MoneyEnum.TWENTYPENCE) == moneyInserted);
		moneyInserted += MoneyEnum.FIFTYPENCE.getNumVal();
		assertTrue(machine.insertMoney(MoneyEnum.FIFTYPENCE) == moneyInserted);
		moneyInserted += MoneyEnum.ONEPOUND.getNumVal();
		assertTrue(machine.insertMoney(MoneyEnum.ONEPOUND) == moneyInserted);
	}

	@Test
	public void returnMoney() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 0;
		moneyInserted += MoneyEnum.TENPENCE.getNumVal();
		assertTrue(machine.insertMoney(MoneyEnum.TENPENCE) == moneyInserted);
		assertTrue(machine.returnMoney() == moneyInserted);
		assertTrue(machine.insertMoney(MoneyEnum.TENPENCE) == moneyInserted);
	}

	@Test
	public void buyItemA() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 100;
		assertTrue(machine.insertMoney(MoneyEnum.ONEPOUND) == moneyInserted);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_A) == (moneyInserted - ItemsEnum.ITEM_A.getItemCost()));

	}

	@Test
	public void buyItemB() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 100;
		assertTrue(machine.insertMoney(MoneyEnum.ONEPOUND) == moneyInserted);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_B) == (moneyInserted - ItemsEnum.ITEM_B.getItemCost()));

	}

	@Test
	public void buyItemC() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 230;
		machine.insertMoney(MoneyEnum.ONEPOUND);
		machine.insertMoney(MoneyEnum.FIFTYPENCE);
		machine.insertMoney(MoneyEnum.FIFTYPENCE);
		machine.insertMoney(MoneyEnum.TWENTYPENCE);
		machine.insertMoney(MoneyEnum.TENPENCE);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_C) == (moneyInserted - ItemsEnum.ITEM_C.getItemCost()));

	}

	@Test
	public void buyItemANotEnoughMoney() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 20;
		assertTrue(machine.insertMoney(MoneyEnum.TWENTYPENCE) == moneyInserted);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_A) == moneyInserted);

	}

	@Test
	public void buyItemBNotEnoughMoney() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 50;
		assertTrue(machine.insertMoney(MoneyEnum.FIFTYPENCE) == moneyInserted);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_B) == moneyInserted);

	}

	@Test
	public void buyItemCNotEnoughMoney() {
		VendingMachine machine = createVendingMachineOn();
		int moneyInserted = 130;
		machine.insertMoney(MoneyEnum.FIFTYPENCE);
		machine.insertMoney(MoneyEnum.FIFTYPENCE);
		machine.insertMoney(MoneyEnum.TWENTYPENCE);
		machine.insertMoney(MoneyEnum.TENPENCE);
		assertTrue(machine.buyItem(ItemsEnum.ITEM_C) == moneyInserted);
	}

	private VendingMachine createVendingMachineOn() {
		VendingMachine machine = new VendingMachine();
		machine.setOn();
		return machine;
	}
}
