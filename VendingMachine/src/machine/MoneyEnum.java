package machine;

public enum MoneyEnum {
	TENPENCE(10), TWENTYPENCE(20), FIFTYPENCE(50), ONEPOUND(100);

	private int moneyVal;

	MoneyEnum(int moneyVal) {
		this.moneyVal = moneyVal;
	}

	public int getNumVal() {
		return moneyVal;
	}

}