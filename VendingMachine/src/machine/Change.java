package machine;

public class Change {

	private static Change changeObj;

	private int tenPence;

	private int twentyPence;

	private int fiftyPence;

	private int onePound;

	private Change() {
		onePound = 5;
		fiftyPence = 5;
		twentyPence = 5;
		tenPence = 5;
	}

	public static Change getInstance() {
		if (changeObj == null) {
			changeObj = new Change();
		}
		return changeObj;
	}

	public int getTenPence() {
		return tenPence;
	}

	public void incrementTenPence() {
		this.tenPence++;
	}

	public void decrementTenPence() {
		this.tenPence--;
	}

	public int getTwentyPence() {
		return twentyPence;
	}

	public void incrementTwentyPence() {
		this.twentyPence++;
	}

	public void decrementTwentyPence() {
		this.twentyPence--;
	}

	public int getFiftyPence() {
		return fiftyPence;
	}

	public void incrementFiftyPence() {
		this.fiftyPence++;
	}

	public void decrementFiftyPence() {
		this.fiftyPence--;
	}

	public int getOnePound() {
		return onePound;
	}

	public void incrementOnePound() {
		this.onePound++;
	}

	public void decrementOnePound() {
		this.onePound--;
	}
}
