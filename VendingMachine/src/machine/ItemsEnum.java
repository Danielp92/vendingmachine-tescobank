package machine;

public enum ItemsEnum {
	ITEM_A(60), ITEM_B(100), ITEM_C(170);

	private int itemCost;

	ItemsEnum(int itemCost) {
		this.itemCost = itemCost;
	}

	public int getItemCost() {
		return itemCost;
	}

}
